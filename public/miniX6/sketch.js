let radio;
let slider;
let checkbox;
let button;

function setup() {
createCanvas(windowWidth, windowHeight);
background(100,100,500,40);
button = createButton('Reflect');
button.position(5, 680);
button.mousePressed(changeBG);
}

function draw(){
  textSize(20);
  fill(0, 102, 153);
  text("Self reflection form", 5, 20, [600], [500]);
  text("for filling out when moody", 5, 35, [600], [500]);
textSize(15);
fill(0, 102, 153);
text("Declare number of friends", 5, 60, [600], [500]);
radio = createRadio();
radio.position(5,80);
radio.option('<9   ');
radio.option('10-19');
radio.option('20-29');
radio.option('30-39');
radio.option('40-49');
radio.option('50-59');
radio.option('60-69');
radio.option('70-79');
radio.option('80-89');
radio.option('90-99');
radio.option('>100');
radio.style('width', '60px');

text("Did you talk to other people today?", 5, 320, [600], [500]);
checkbox = createCheckbox('Yes', false);
checkbox.position(5, 340);
checkbox = createCheckbox('No', false);
checkbox.position(5, 360);

text("Are you tired?", 5, 400, [600], [500]);
checkbox = createCheckbox('Yes', false);
checkbox.position(5, 420);
checkbox = createCheckbox('No', false);
checkbox.position(5, 440);

text('Are you hungry?', 5, 480, [600], [500]);
checkbox = createCheckbox('Yes', false);
checkbox.position(5, 500);
checkbox = createCheckbox('No', false);
checkbox.position(5, 520);

text("Have you been active today?", 5, 570, [600], [500]);
text("Very active", 5, 590, [600], [500]);
text("Not active at all", 175, 590, [600], [500]);
  slider = createSlider(0, 255, 100);
  slider.position(85, 590);
  slider.style('width', '80px');

  text("How was your overall mood?", 5, 620, [600], [500]);
  text("Great", 5, 640, [600], [500]);
  text("Garbage", 140, 640, [600], [500]);
    slider = createSlider(0, 255, 100);
    slider.position(50, 640);
    slider.style('width', '80px');
}

function changeBG() {
  let val = random(255,0,0);
  background(val);
    fill(255,0,0);
    noStroke();
    text("u suck",random(windowWidth),random(windowHeight),50,50);
}
