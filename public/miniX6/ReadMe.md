**Self Reflection Form**

Here is my program: https://sofienisted.gitlab.io/aesthetic-programming/miniX6/

For some reason, I can't upload my files from my miniX6 folder, but I am going to fix it within 22/3

![](miniX6.1.png)

This first screenshot is what is seen when the program is opened.

![](miniX6.2.png)

This screenshot is what is/could be seen after the "Reflect"-button is pressed.

I wanted to rework my miniX4, because I got feedbck that week, and I wanted to work on some of those points.
I had made a form to be filled out with personal informations that were supposed to determine a person's worth, 
but as the feedback goup pointed out, nothing actually happened, when the form had been filled.
For this iteration, I made what looks like a submit button, but because I chose a different theme - that of 
self reflection and self devaluation - it is called "Reflect" instead of "Submit".
When the button is pressed, the background is changed to a random greyscale colour, and the words "u suck" appear in bright red.
However the form is filled, the result will be a greyscale background and "u suck" in red, as to reflect how external circumstances aren't always the reason for feeling bad about yourself. 
I also wanted to include the random syntax, because I recently learned how to use it, and I think it's very cool:)

I have enjoyed how the course aesthetic programming hasn't just been about becoming the best at programming, 
but also how to apply these new skills to different questions and delving into new ways of thinking. 
For instance, my miniX6 is supposed to represent the kind of self reflection one might do after a long, difficult day,
but made to look like an internet form. The fact that the outcome ("u suck") can't be changed through the form, a long with how the reason for the statement is outside of the user's understanding, is symbolic of how, despite our best efforts,
we can't always solve, or even understand, our own mental states.
This also shows the program's relation to digital culture, as the everpresent GUIs make it tempting to simplify everything,
even our emotions.

Programming in p5.js with the program in the browser next to the Atom window makes it really easy to tinker with different
parameters. This is very helpful, when trying to design a program. For this miniX (and the one it is based on), I did this a 
lot, as I had wanted to keep approximately the same distance from word to word, and checkbox to checkbox.
