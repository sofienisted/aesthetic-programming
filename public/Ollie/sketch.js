var x=0;
var y=0;
var spacing=30;

function setup(){
  createCanvas(windowWidth,windowHeight);
  background(255);
}

function draw(){
  noStroke();
  if(random(1)<0.5){
    fill(255,0,0,);
    text("Ollie",x+spacing,y+spacing,200,200);
  }else{
        fill(0,0,255,);
    text("Ollie",x+spacing,y+spacing,200,200);
  }
  x=x+spacing;
  if(x>width){
    x=0;
    y=y+spacing;
  }
  x=x+spacing;
  if(y>height){
    x=x+spacing;
    y=0;
  }

  for(var i=0;i<1;i++){
    fill(255,0,0,20);
    noStroke();
    ellipse(random(windowWidth),random(windowHeight),50,50);
  }
  for(var i=0;i<1;i++){
    fill(0,0,255,20);
    noStroke();
    ellipse(random(windowWidth),random(windowHeight),50,50);
  }
}
