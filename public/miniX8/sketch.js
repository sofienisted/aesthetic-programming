let button;

function preload() {
  missinformation = loadJSON('missinformation.json');
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(200);

  button = createButton('Click here');
    button.size(90, 40);
    button.mousePressed(showInformation); //calling the function of showInformation
    button.position(windowWidth/2-25, windowHeight/2+40);

    //text
    textAlign(CENTER);
    textSize(62);
    fill(102, 255, 102);
    stroke(146, 214, 16);
    text('MISS INFORMATION', windowWidth/2, 100);
    textSize(40);
    text('Someone knows more than you', windowWidth/2, 150);
    text('Press the button to find out what', windowWidth/2, windowHeight/2);
}

function showInformation() {
  removeElements();
  background(146, 214, 16);

  button = createButton('New'); //creating button for new statement
    button.size(90, 40);
    button.mousePressed(showInformation);
    button.position(windowWidth-100, windowHeight-50);

  let i = floor(random(0, 38));
  let j = floor(random(0, 33));
  let k = floor(random(0, 34));

  fill(50);
  textSize(30);
  //adding the text with randomized index number for different statements
  text("Put " + missinformation.noun[i] + " in your " + missinformation.placement[j] + " for " + missinformation.result[k], windowWidth/2, windowHeight/2);
}
