Here is my program: https://sofienisted.gitlab.io/aesthetic-programming/miniX7/

**Rabbit's Game**

![](rabbitsgame.png)

This game is dedicated to the sick rabbit, we had to kill at my job last week :(

I started by working with the source code for Flappy Bird, and changed the look of it with images. 
As is the case with Flappy Bird (on a pc), you press the spacebar to make the object go higher and escape
the obstacles. I later added in the carrots, which are based on the tofu from Tofu Go!, to include a class.
Whereas the tofu is made with shapes from the p5.js library, the carrots are (like the rest of my program) 
made using images, because I wanted a watercolour look. When the rabbit hits the shovel-obstacles, the word "Av" 
appears in red, and a count is being kept of the player's shovel-hits. There is also a count of carrots eaten.
I decided to keep the condition from Tofu Go!, stating that once the "lose"-count surpases the "win"-count, and 
the "lose"-count is larger than two, the game will be over.

**I couldn't get the carrots to move along the the x-axis, but even though they are "shown" they won't "move". I hope to get it to work soon, but the following paragraph is written as if the program actually works as intended**

By using the OO approach, the carrots are able to move at different speeds and from different y-positions, 
but still clearly belong to the same class. 
Since I am using these watercolour images for my program, I have decided to let other people abstract the objects
of my game. It is still "_negotiating with the real world_" (Soon & Cox, 2020, p.145), but I am not the one doing the negotiating. Even
though the images are arguably more realistic than a set of shapes from the p5.js library, they are still just images.
Their role in this game is for the player (the subject) to recognise them (the objects) as carrots, and play the game 
accordingly.

![](corpart.png)

In relation to the question of abstraction, I started thinking about the newly popular "corporate art style" (example above), which has been used on many different platforms, among which Youtube has been one of the most frequent users.
It has been criticized for being an overly simplified abtraction of the human body for the sake of being just that, but that is exactly where I believe the value of the style lies. The shape of the characters vaguely resemble the human body, with (most of) them having arms, legs and a head. Although, the style refrains from giving a certain bodytype or skin colour status above any other by giving it's objects skin colours like blue or purple or legs that look like thick noodles. This is an example of a situation where I personally think that abstraction has been used for good by elevating inclusivity. 

**References**

Youtube, The Coding Train. 2016. "Coding Challenge #31: Flappy Bird". https://www.youtube.com/watch?v=cXgA1d_E-jY&t=193s. Last accessed 28/3/2021.

TofuGo! source code repository: https://gitlab.com/aesthetic-programming/book/-/tree/master/public/p5_SampleCode/ch6_ObjectAbstraction


Soon, Winnie and Cox, Geoff. 2020. Aesthetic Programming: A Handbook of Software Studies. London, Open Humanities Press, p. 148.
