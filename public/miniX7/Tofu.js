/*create a class: template/blueprint of objects
with properties and behaviors*/

class Tofu {
    constructor()
    { //initalize the objects
    this.speed = floor(random(3, 6));
    //check this feature: https://p5js.org/reference/#/p5/createVector
    this.pos = new createVector(width-100, random(12, height/1.7));
    this.size = floor(random(15, 35));
    //rotate in clockwise for +ve no
    this.tofu_rotate = random(0, PI/20);
    }
    move() {  //moving behaviors
      this.pos.x-=this.speed;  //i.e, this.pos.x = this.pos.x - this.speed;
    }
  show() { //show tofu as a cube
    push()
    translate(this.pos.x, this.pos.y);
    rotate(this.tofu_rotate);
    image(img4,0, 0, 100, 30);
    pop();
 }
}
