// Daniel Shiffman
// http://codingtra.in
// http://patreon.com/codingtrain
// Code for: https://youtu.be/cXgA1d_E-jY&
var bird;
var birdSize={
  w:100,
  h:100
};
var birdPosY;
var pacman;
var mini_height;
var pipes = [];
var img, img1, img2, img3, img4;
var min_tofu=5;
var tofu=[];
var score=0,lose=0;
var keyColor=0;

function preload(){
  img=loadImage("forest.jpeg");
  img1=loadImage("skovlned.png");
  img2=loadImage("skovlop.png");
  img3=loadImage("Preben.png");
  img4=loadImage("carrot.png");
}

function setup() {
  createCanvas(640, 480);
  bird = new Bird();
  pipes.push(new Pipe());
}

function draw() {
  background(0);
  image(img,0, 0, 700, 500);
  displayScore();
  checkTofuNum(); //available tofu
  showTofu();
  checkEating(); //scoring
  checkResult();

  for (var i = pipes.length - 1; i >= 0; i--) {
    pipes[i].show();
    pipes[i].update();

    if (pipes[i].hits(bird)) {
      console.log('HIT');
      textSize(50);
      text("AV", 200, 200);
    }

    if (pipes[i].offscreen()) {
      pipes.splice(i, 1);
    }
  }

  bird.update();
  bird.show();

  if (frameCount % 75 == 0) {
    pipes.push(new Pipe());
  }
}

function checkTofuNum(){
  if (tofu.length < min_tofu) {
    tofu.push(new Tofu());
  }
}

function showTofu(){
  for (let i = 0; i <tofu.length; i++) {
      tofu[i].move();
      tofu[i].show();
    }
}

function checkEating() {
  //calculate the distance between each tofu
  for (let i = 0; i < tofu.length; i++) {
    let d = int(dist(birdSize.w/2, birdPosY+birdSize.h/2,
        tofu[i].pos.x, tofu[i].pos.y)
      );
    if (d < birdSize.w/2.5) { //close enough as if eating the tofu
      score++;
      tofu.splice(i,1);
    if (pipes[i].hits(bird)) {
      lose++;
      pipes.splice(i,1);
    }
  }
}
}

function displayScore() {
    fill(keyColor, 255);
    textSize(20);
    text('You have eaten '+ score + " carrot(s)", 10, 400);
    text('You have taken ' + lose + " shovel-hits(s)", 10, 420);
    fill(keyColor,255);
    text('PRESS SPACE to escape the shovels and eat the carrots', 10, 450);
}

function checkResult() {
  if (lose > score && lose > 2) {
    fill(keyColor, 255);
    textSize(26);
    text("The rabbit has died...GAME OVER", width/3, height/1.4);
    noLoop();
  }
}

function keyPressed() {
  if (keyCode == 32 ) {
    bird.up();
    //console.log("SPACE");
  }
}
