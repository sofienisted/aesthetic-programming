let x=0;
let y=0;
let specing=10;

function setup(){
  createCanvas(windowWidth,windowHeight);
  background(255);
}

function draw(){
  stroke(0);
  if (random(1)<0.5){
    line(x,y,x+spacing,y+spacing);
  }else{
    line(x,y+spacing,x+spacing,y);
  }
  x+=10;
  if(x>width){
    x=0;
    y+=spacing;
  }
}
