**The Gyphyfier**

Run the program: https://sofienisted.gitlab.io/aesthetic-programming/minix9/

![](minix9.gif)

_The button next to the search bar is purely ornamental, 
as the program will search anything as soon as it is typed_


**What is the program about? Which API have you used and why?**
Our program collects gifs from Giphy, and the user is able to search for gifs using a search bar. The user searches a word, and is presented with three gifs relating to it. We chose the Giphy API after trying out various others (Spotify, Genius and others), where we found the others to be too complicated. The Giphy API was in our opinion quite easy to use, and The Coding Train also provided a good tutorial. This week we definitely started with high ceilings and wanted to work with lyrics of hip-hop songs, but after searching the heck out of youtube and google to find a slightly understandable tutorial for the API’s we originally wanted, we decided on going with the Giphy one, because we could use the same image format as we used when trying out Winnie’s google API sample code.
							 								
**Can you describe and reflect on your process in this miniX in terms of acquiring,
processing, using, and representing data? How much do you understand this data or what do you want to know more about? How do platform providers sort the data and give you the requested data? What are the power relations in the chosen APIs? What is the significance of APIs in digital culture?**
When we searched for the Giphy api, the site provided a good tutorial and you only needed to login/create a user (for free) to get an api key. From watching the Coding Train’s video on giphy’s web api specifically, we got a good understanding of how the data is acquired, and how the JSON is similar to what we have worked with last week. We based our minix in the search function, and they’re drawn from their urls. The gifs are sorted in arrays and tagged with different words. When the user inputs a word, the gifs are checked for tags, and the three first such tagged gifs are displayed in positions designated by us. Sometimes, the shown gifs seem kind of random, but this is because of how Giphy (and the users uploading and creating gifs) has tagged their gifs in the API, so that is outside of our program’s power. This could potentially be misused, as some people have the power to tag the gifs, while others will have to accept these tags in order to find and use the gifs. This mechanism is also described by Soon and Cox: “In a query-driven society, search engines have become powerful mechanisms for truth-making” (Soon & Cox, 2020, p.189), underlining the power held by those who get to define data.
Giphy is the leading provider of gifs, and is used by Facebook (Messenger, Instagram). For our MiniX, we probably won’t exceed the request limit, but when large companies want to use this data, they will have to pay for it. As mentioned, Giphy is a so-called community, where users contribute to the database, after which Giphy makes money from the API’s. Since the platform is free to use, and the company has expenses, it makes sense that they would have to earn back that money, but without knowing about API’s, the users might not be aware of this dynamic.
API’s are useful tools to retrieve data from a site/app and use it in your own site/app, and they bridge the communication between the two. API’s also provide a look into how different platforms work, and as with Giphy, what kind of rules decide what the user sees, as Snodgrass and Soon discuss: “By studying and paying attention to the makeup of their structure and parameters one can also begin to detect the priorities, and thus politics, of the platforms in question.” (Snodgrass & Soon, 2019, n/p). When we started working with our minix, we initially wanted to work with hip-hop music, sampling and copyright, which we found super relevant for the theme of API. The hip-hop culture is famously known for and built upon musicians sampling other songs, but oftentimes does not give credit. We thought of the notion of API’s in a similar way, as they give the user an opportunity to take something from another site and present it as their own. Digital media thrive from sharing and contributing together with the users, and though guidelines and terms and conditions present clear rules for what is accepted in terms of copying and borrowing/taking, a certain trust is put on the users. And with this in mind, we think that API’s are amazing tools for this reason, because they open up for creativity and freedom when designing and creating online.
				 								
**Try to formulate a question in relation to web APIs or querying/parsing processes that you would like to investigate further if you had more time.**
It would be interesting to look more into the legality and/or profit of the API’s used by large companies and websites. During the past week, it has become clear that API’s are not an insignificant part of the online economy, so it would be interesting to see how much of a company's revenue is from sharing all this data. When looking into Giphy and the relation between contributors and the company’s API distribution, it became clear that Giphy reserves the right to freely use anything uploaded to their platform and with no compensation whatsoever. However, it is written on their Terms of Service page that the user owns the gifs they produce, but it is unclear what this means in terms of legality and copyright, and this would also be interesting to look further into.
				
**References:**

Soon Winnie & Cox, Geoff, "Que(e)ry data", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 186-210

Eric Snodgrass and Winnie Soon, “API practices and paradigms: Exploring theprotocological parameters of APIs as key facilitators of sociotechnical forms of exchange],”First Monday 24, no.2 (2019), https://journals.uic.edu/ojs/index.php/fm/article/view/9553/7721.

https://developers.giphy.com/docs/api (last accessed 18.04.21)

The Coding Train, video 10:10 “The Giphy API and Javascript - p5.js Tutorial”, https://www.youtube.com/watch?v=mj8_w11MvH8&t=108s (last accessed 16.04.21)

