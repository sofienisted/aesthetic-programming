**Declare Value**

![](form.png)

Edit: I figured it out! Here is my program: https://sofienisted.gitlab.io/aesthetic-programming/miniX3/


I wanted to create a parody of this type of form. It is often used to capture information about
a user before they get access to a website, but checklists and multiple choice questionnaires also exist in meatspace. To many people, the very limited number of options feels rigid, especially when the questions concern identity. I also wanted to highlight the fact that some
people are both the preferred and asumed user of a given program/webpage.
The title "Declare Value" has a dual meaning, as the user declares actual values (like their age), but the program promises to declare the value of the user as a person.

My program is a checklist with radio buttons, checkboxes and slider bars. 
The reason why I have included both radio buttons and checkboxes, is because the radio buttons would toggle back and forth whenever I pressed a new one. This makes sense in the top question about age, as you can't choose more than one ten-year-age span, and I would have liked to use the feature for several of the questions to underline my program's point about rigidity. However, I couldn't seem to get the radio buttons to ignore the buttons used for the other questions. I tried writing them as different functions. That didn't work. 
In regards to slider bars, I just really wanted to make them, and I personally thought it was funny how either end of the bar said the same thing:)

My program isn't about how data is cleverly being harvested off of us, but more about what 
the end result is. Certain groups are clearly advantaged, and this is in part reinforced by the many ways in which we are categorized. When we aren't being watched by cctv or analyzed by 
algorithms, we are asked to actively categorize ourselves to access many a website or social 
media platform. Keeping the more discreet types of surveillance in mind, these obvious ones seem sinister.

Here is my program: https://sofienisted.gitlab.io/aesthetic-programming/miniX4/

