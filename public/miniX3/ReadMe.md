![](ring.png)

For this miniX, I wanted to make a throbber that would fool people 
into thinking that their desired content is being loaded.
During the past week, I was told that pages like Facebook use 
throbbers and signify loading, in order to make the user believe 
that the page is loading something in order to enhance the user experience.
In reality, the content is loaded much faster than we are made to believe. 
I would argue that people would prefer to get their content instantly, 
which is why, when you click the screen in my program, a screenshot 
of my Facebook feed is shown. Once again, the url doesn't work, 
so I left the screenshots in my miniX3 repository. 
I tried getting help from the instructors and my study group, 
but it still doesn't work. 
Please send help - Sofie

Edit: I figured it out! Here is my program: https://sofienisted.gitlab.io/aesthetic-programming/miniX3/
