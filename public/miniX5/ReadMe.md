**Bubbles**

Here is my program: https://sofienisted.gitlab.io/aesthetic-programming/miniX5/

![](bubbles1.png)
![](bubbles2.png)

For this miniX I wanted to make shapes of different colours and I wanted the shapes to continue looping on top of each other.
Therefore, I made two rules:
1)50% of the time, the program should draw a red ellipse. The rest of the time, it should draw a blue one.
2)When the ellipses reach the bottom right corner of the window, the ellipses should loop and draw again from the top left corner.
I added a alpha value to both ellipses in order to be able to see them layer on top of each other. 
Because of this, the colours will, over time, create a more homogenous look than just red/blue.
For the larger ellipses that are organized in lines, I used an if-statement to make them loop. 
In order to also include a loop, I made the smaller ellipses in a for(loop), also using the random() syntax, to make them pop up
in random positions, as opposed to the larger ellipses.
I originally worked from Daniel Shiffman's video "Coding Challenge #76: 10PRINT in p5.js" to make the 10PRINT for myself.
From there, I changed and added different things. 10PRINT and my program are similar in the sense that they create a particular look 
after being run for a certain amount of time - this is their emergent behaviour. I think my program ressembles Langton's Ant more, though. At least as far as the look of the output goes. Both programs start from blank and quickly accumulate a pattern, after which the pattern takes over and almost becomes indecipherable. 

In relation to the question of auto-generating, this programs element of auto-generating lies ine the fact that, beyond initialising the program, the user has no control over the outcome. Since the positions of the ellipses are randomly generated, there is no way to predict where the next one will appear or how exactly the final pattern will look. I personally think this is cool, because it makes my patterns look more "organic".


**References:**

Youtube, The Coding Train. 2017. "Coding Challenge #76: 10PRINT in p5.js" by The Coding Train. Link: https://www.youtube.com/watch?v=bEyTZ5ZZxZs&t=313s, last accessed 14/3/2021. 
