My first sketch!
What have I produced?
I have produced a series of photographs of my friends, which can be "flipped" through using a button with the text "press for good friends". I thought it was funny, because you don't get friends, you get pictures of friends (like "Ceci n'est pas une pipe" by Magritte).
How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?
I have only ever tried making something using visual computing in Scratch, so making the code myself was very challenging. I used references in the p5.js library to make the button as well as changing colours. I also leaned heavily on my study group as well as the instructors for guidance, which was very helpful. Having the coding language explained by other humans definetely made it easier.
How is the coding process different from, or similar to, reading and writing text?
The code isn't legible in the same way as text, as it doesn't make sense at first sight - at least not to me at this point. In a way, it ressembles the experience of learning a new language, complete with frustration and the feeling of joy, when it starts to make sense to you.
What does code and programming mean to me, and how does the assigned reading help me to further reflect on these terms?
I have always thought of coding as something unobtaible and reserved for certain geniuses, so when I finally got the button to work, it was very empowering. I do realise that there are a number of things that I could have elaborated and/or polished a bit more, and there were ideas I discarded, because I found them too challenging, but I am looking forward to learning more and making cooler stuff.
