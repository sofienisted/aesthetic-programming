function setup(){
  createCanvas(640,480);
  print("hello world");
  button1=createButton(":)");
  button1.position(100, 250);
  button1.mousePressed(lovetext);

  button2=createButton(":(");
  button2.position(500, 250);
  button2.mousePressed(hugtext);
}

function draw(){
  background((50));
  let c = color(320, 130, 30);
  fill(c);
  ellipse(320,240,200,200);

  let c1 = color(0);
  fill(c1);
  ellipse(270,230,20,40);

  let c2 = color(0);
  fill(c2);
  ellipse(370,230,20,40);

  let c4 = color(255, 204, 0);
  fill(c4);
  ellipse(320,310,120, 90);

  let c3 = color(0);
  fill(c1);
  ellipse(320,290,20,10);

  noFill();
arc(320, 290, 90, 90, 0, PI, TWO_PI);

fill(c);
arc(390, 175, 80, 80, 10, 1, PI + QUARTER_PI, CHORD);

fill(c);
arc(280, 190, 100, 100, 650, 200, PI, TWO_PI);
}

function lovetext(){
  textSize(30);
  fill(255,204,400);
  text("monkey",195,117);
}

function hugtext(){
  textSize(30);
  fill(140,255,0);
  text("hug",195,117);
}
