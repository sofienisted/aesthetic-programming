For this miniX, I wanted to make the face of a monkey in the distinctive emoji-style. 
The buttons on the left and right of the face show the emoticons ":)" and ":(". 
The idea of this was that, when pressed, the buttons would produce the text "Monkey loves you" and "Monkey needs a hug",  respectively.
This comes from the an episode of Black Mirror (Black Museum), where a mother's consciousness is uploaded to a monkey-looking teddy bear, in order for her to be closer to her son. 
From inside the monkey, she has no freedom of movement, and she can only display the two emotions, happy and sad, in the form of a sound-output: "monkey loves you"/"monkey needs a hug". 
I think this highlights the question of which emotions are represented in the set of emojis. 
By now, we have many emojis so obscure that I don't even know what they're supposed to mean, or they have introduced us to feelings, we didn't even know we had. 
With the latter, I am thinking of the upside-down smiley, whose interpretations are as many as there are users of it. 
Even though this is true, describing your feelings with icons, pre-designed by other people, can be challenging - and I imagine even more so, back when the emoticons were fewer. 
In the episode, not being able to explain yourself was shown as torture and inhumane, which is, of course, taken to the extreme. 
But I do think that the more we lean on emojis, the less we are able to rely on our own writing-abilities.
I made my monkey because I can't stop thinking about this episode, and I just wanted to make something simple given my lack of experience. Unfortunately, I couldn't get the buttons to display the text (even though it worked fine the last time). The left ear also looks weird. There is a screenshot of the "program" in the miniX2 folder, because I forgot to ask how to share the link. 
