var img;
function preload(){
  img=loadImage("fb");
}

function setup(){
  createCanvas(windowWidth, windowHeight);
  frameRate(8);
}

function draw(){
  background(70,80);
  drawElements();
}

function drawElements(){
  let num=9;
  push();
  translate(width/2,height/2);
  let cir=360/num*(frameCount%num);
  rotate(radians(cir));
  noStroke();
  fill(255,255,255);
  ellipse(35,0,22,22);
  pop();
  stroke(255,255,0,18);
}

function windowResized(){
  resizeCanvas(windowWidth,windowHeight);
}

function mousePressed(){
  createCanvas(windowWidth, windowHeight);
  frameRate(0);
  image(img,0,0,windowWidth, windowHeight);
}
